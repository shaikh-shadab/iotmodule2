# IIoT Protocols

##  **4-20 mA current loops**

* The 4-20 mA current loop has been the standard for signal transmission and electronic control in control systems. 
   In a current loop, the current signal is drawn from a dc power supply, flows through the transmitter,
   into the controller and then back to the power supply in a series circuit.
* The advantage is that the current value does not degrade over long distances, so the current signal remains constant through all components in the loop.
* As a result, the accuracy of the signal is not affected by a voltage drop in the interconnecting wiring.

![4-20 mA](https://www.azom.com/images/Article_Images/ImageForArticle_15057(1).jpg)

  **Advantages**
* The 4-20 mA current loop is the dominant standard in many industries.
* It is the simplest option to connect and configure.
* It uses less wiring and connections than other signals, greatly reducing initial setup costs.
* Better for traveling long distances, as current does not degrade over long connections like voltage.
* It is less sensitive to background electrical noise.
* Since 4 mA is equal to 0% output, it is incredibly simple to detect a fault in the system.

  **Disadvantages**
* Current loops can only transmit one particular process signal.
* Multiple loops must be created in situations where there are numerous process variables that require transmission. Running so much wire could lead to problems with ground loops if independent loops are not properly isolated.
* These isolation requirements become exponentially more complicated as the number of loops increases.


##  **MODBUS**
* MODBUS Protocol is a messaging structure, widely used to establish master-slave communication between intelligent devices.
* The devices include a register map outlining where the configuration, input and output data can be written and read from.
* The device requesting the information is called the Modbus Master and the devices supplying information are Modbus Slaves. 

  ![Modbus](https://domat-int.com/wp-content/uploads/15_vice_klientu1.png)

**HOW DOES MODBUS PROTOCOL WORKS?**

* Communication between a master and a slave occurs in a frame that indicates a function code.
* The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.
* The slave then responds, based on the function code received.
* The protocol is commonly used in IoT as a local interface to manage devices.
* Modbus protocol can be used over 2 interfaces. They are -

    1. RS485 - called as Modbus RTU - RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.
    2. Ethernet - called as Modbus TCP/IP - Ethernet protocol is a typical LAN technology


### RS485

* RS485 is a serial data transmission standard widely used in industrial implementations. 
* RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.
* RS485 is not directly compatible: you must use the correct type of interface, or the signals won't go through. Mainly done through an easy to use an RS485 to USB.


### How RS485 MODBUS Protocol works?
  The MODBUS RS485 protocol defines communication between a host (master) and devices (slaves) that allows querying of device configuration and monitoring.
  MODBUS messages relay simple read and write operations on 16 bit words and binary registers often referred to as “coils”


## **OPC UA**
* Machine to machine communication protocol for industrial automation developed by the OPC Foundation.
* Focus on communicating with industrial equipment and systems for data collection and control.
* Implements a sophisticated Security Model that ensures the authentication of Client and Servers, the authentication of users and the integrity of their communication.
  OPC UA is designed to connect Objects in such a way that true Information can be shared between Clients and Servers.

![opcua](https://integrationobjects.com/images/2016/04/UA-Client-Architecture.png)

# Cloud Protocols

## **MQTT**

* MQTT (MQ Telemetry Transport) is a lightweight messaging protocol that provides resource-constrained network clients with a simple way to distribute telemetry information. 
* The protocol, which uses a publish/subscribe communication pattern, is used for machine-to-machine (M2M) communication and plays an important role in the internet of things (IoT).
* The MQTT protocol is a good choice for wireless networks that experience varying levels of latency due to occasional bandwidth constraints or unreliable connections.
* The MQTT protocol surrounds two subjects: a client and a broker.
* An MQTT broker is a server, while the clients are the connected devices. 
* When a device or client wants to send data to a server or broker it is called a publish. 
* When the operation is reversed, it is called a subscribe.

### Working 

* MQTT Client as a publisher sends a message to the MQTT broker whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.
* Topics are a way to register interest for incoming messages or to specify where to publish the message. 
* Represented by strings, separated by forward slash. 
* Each slash indicates a topic level.

![MQTT work](https://i.ytimg.com/vi/EIxdz-2rhLs/maxresdefault.jpg)

## **HTTP**

* The Hypertext Transfer Protocol (HTTP) is an application protocol for distributed, collaborative, hypermedia information systems. 
* HTTP is the foundation of data communication for the World Wide Web, where hypertext documents include hyperlinks to other resources that the user can easily access. 
* For example by a mouse click or by tapping the screen in a web browser.

### Working 

* As a request-response protocol, HTTP gives users a way to interact with web resources such as HTML files by transmitting hypertext messages 
  between clients and servers.
* HTTP clients generally use Transmission Control Protocol (TCP) connections to communicate with servers.

### The Request

 Request has 3 parts

* Request line
* HTTP headrs
* message body

**GET request**
 It is a type of HTTP request using the GET method.

 HTTP utilizes specific request methods in order to perform various tasks

* GET requests a specific resource in its entirety.
* HEAD requests a specific resource without the body content.
* POST adds content, messages, or data to a new page under an existing web resource.
* PUT directly modifies an existing web resource or creates a new URL if needed.
* DELETE gets rid of a specified resource.
* TRACE shows users any changes or additions made to a web resource.
* PTIONS shows users which HTTP methods are available for a specific URL.
* CONNECT converts the request connection to a transparent TCP/IP tunnel.
* PATCH partially modifies a web resource.

### The Response

  Response is made of 3 parts

* Status line
* HTTP header
* Message body




