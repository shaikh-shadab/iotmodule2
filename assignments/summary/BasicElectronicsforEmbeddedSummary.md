# Basic Electronics for Embedded systems

# **Sensors and Actuators**

### Sensors

A sensor monitors environmental conditions such as fluid levels, temperatures, vibrations, or voltage.
When these environmental conditions change, they send an electrical signal to the sensor.
![](https://microcontrollerslab.com/wp-content/uploads/2019/02/Difference-between-sensors-and-actuators.gif)

 **Types of sensors**

- Temperature sensors
* Vibration sensors
* Security sensors
* Pressure sensors
* Humidity sensors
* Motion sensors
* Light sensors
* Image sensors
* Gas sensor
* Smoke Sensor
* Chemical IoT sensor


 **Few Actuators**

* Linear actuators
* Motor actuators
* Relays actuators
* Solenoids actuators
* Manual actuators
* Pneumatic actuators
* Hydraulic actuators
* Electric actuators
* Spring actuators

# **ADC & DAC**

| _Analog_ | _Digital_|
|------- |--------|
| An analog signal is a continuous signal that represents physical measurements.| Digital signals are time separated signals which are generated using digital modulation. |
| Denoted by sine waves | Denoted by square waves |
| It uses a continuous range of values that help you to represent information. | Digital signal uses discrete 0 and 1 to represent information. |
| Temperature sensors, FM radio signals, Photocells, Light sensor, Resistive touch screen are examples of Analog signals. | Computers, CDs, DVDs are some examples of Digital signal.|
| The analog signal bandwidth is low. | The digital signal bandwidth is high. |
| It is suited for audio and video transmission. | It is suited for Computing and digital electronics. |
| Analog signal doesn't offer any fixed range. | Digital signal has a finite number, i.e., 0 and 1. |




# **MICROPROCESSORS & MICROCONTROLLERS**

| _MicroProcessors_ | _MicroControllers_ |
|------- |--------|
| consists of only a Central Processing Unit | contains a CPU, Memory, I/O all integrated into one chip |
| uses an external bus to interface to RAM, ROM, and other peripherals | uses an internal controlling bus |

## When to use ?

For small applications where only few basic task to be done ,a microcontroller can be used as it comes with all basic components such as RAM,ROM etc in buit on it. Microprocessor has powerful configuration than a Microcontroller.

![](https://www.microcontrollertips.com/wp-content/uploads/2017/10/Fig-1-MicrocontrollerMicroprocesser-opener.jpg)


# **SERIAL & PARALLEL COMMUNICATIONS**

| _Serial Communication_ | _Parallel Communication_ |
|------- |--------|
| Due to the presence of single communication link the speed of data transmission is slow| multiple links in case of parallel communication allows data transmission at comparatively faster rate |
| Upgrading a system that uses serial communication is quite an easy task | Upgradation is difficult as compared |
| the all data bits are transmitted over a common channel thus proper spacing is required to be maintained in order to avoid interference | the utilization of multiple link reduces the chances of interference between the transmitted bits |
| supports higher bandwidth | supports comparatively lower bandwidth |
| efficient for high frequency operation | suitabile in case of low frequency operations |
| suitable for long distance transmission of data | suitable for short distance transmission of data |

![](https://www.codrey.com/wp-content/uploads/2017/09/Serial-Communication.png)

![](https://electricalfundablog.com/wp-content/uploads/2019/01/Parallel-Communication-Data-Flow-Through-Parallel-Paths.png)


# **Raspberry Pi**

The Raspberry Pi is a low cost, small sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse. It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python.

## Pin Diagram

![](https://www.raspberrypi-spy.co.uk/wp-content/uploads/2012/06/Raspberry-Pi-GPIO-Header-with-Photo.png)

It is:-
* Mini Computer
* Limited but large power for its size
* No storage
* It is a SOC (System On Chip)
* We can connect shields (Shields - addon functionalities)
* Can connect multiple Pi’s together
* Microprocessor 
* Can load a linux OS on it
* RPi uses ARM 
* Connect to sensors or actuators 

## Raspberry Pi Parallel Interfaces

### 1.GPIO 

* **GPIO stands for General Purpose Input/Output**. It's a standard interface used to connect microcontrollers to other electronic devices. 
* Connecting a GPIO or other pin to the wrong voltage can easily damage the board and render it unusable. 


## Raspberry Pi Serial Interfaces

### UART

**UART** is a Universal Asynchronous Receiver Transmitter used for serial communication in embedded systems. 
* It has two lines Tx(Transmit) and Rx(Receive). It has a circuitry in micro-controller or has stand alone ICs.
* The electric signaling levels and methods are handled by a driver circuit external to the UART.
* Two UARTs can communicate directly. 
* UART can convert parallel data coming from controller device into the serial data it also add extra bits to it which are start,parity and stop bits.
* When receiving UART detects start bit then it starts receiving data at a specified frequency known as baud rate. Baud rate is a bits per sec.During communication between two UARTs the baud rate must be same.
 
 ![UART1](https://www.codrey.com/wp-content/uploads/2017/10/UART-Block-Diagram.png)

 
#### Steps of UART transmission:
* The transmitting UART receives data in parallel from the data bus.
* The transmitting UART adds the start bit, parity bit, and the stop bit(s) to the data frame.
* The entire packet is sent serially from the transmitting UART to the receiving UART. The receiving UART samples the data line at the pre-configured baud rate.
* The receiving UART discards the start bit, parity bit, and stop bit from the data frame.
* The receiving UART converts the serial data back into parallel and transfers it to the data bus on the receiving end.

#### Advantages:
* Only uses two wires.
* No clock signal is necessary.
* Has a parity bit to allow for error checking.
* The structure of the data packet can be changed as long as both sides are set up for it.
* Well documented and widely used method.



